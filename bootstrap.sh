echo "deb http://ppa.launchpad.net/ansible/ansible-2.10/ubuntu bionic main" >> /etc/apt/sources.list
echo "deb-src http://ppa.launchpad.net/ansible/ansible-2.10/ubuntu bionic main" >> /etc/apt/sources.list
gpg --keyserver keyserver.ubuntu.com --recv-key 6125E2A8C77F2818FB7BD15B93C4A3FD7BB9C367
gpg --export -a 6125E2A8C77F2818FB7BD15B93C4A3FD7BB9C367 | apt-key add -
apt update && apt install -y ansible-base
